from .database import Base
from sqlalchemy import Column, Integer, String, Boolean, Text, ForeignKey, BigInteger
from sqlalchemy.orm import relationship


class AuthorizeInfo(Base):
    __tablename__   = 'authorize_info'

    id              = Column(Integer, primary_key=True, index=True)
    access_token    = Column(String)
    expires_in      = Column(String)


class ProductInfo(Base):
    __tablename__   = 'product_info'

    id              = Column(Integer, primary_key=True)
    body_html       = Column(Text)
    body_plain      = Column(Text)
    created_at      = Column(String(32))
    handle          = Column(String(32))
    product_type    = Column(String(32))
    published_at    = Column(String(32))
    published_scope = Column(String(32))
    tags            = Column(String(64))
    template_suffix = Column(String(32))
    title           = Column(String(64))
    updated_at      = Column(String(32))
    vendor          = Column(String(32))
    only_hide_from_list = Column(Boolean)
    not_allow_promotion = Column(Boolean)


class ProductImages(Base):
    __tablename__   = 'product_images'

    id              = Column(Integer, primary_key=True)
    product_id      = Column(Integer, ForeignKey('product_info.id'))
    created_at      = Column(String(32))
    position        = Column(Integer)
    src             = Column(Text)
    updated_at      = Column(String(32))
    filename        = Column(String(256))

    product         = relationship('ProductInfo', foreign_keys=[product_id])


class ProductVariants(Base):
    __tablename__   = 'product_variants'

    id              = Column(Integer, primary_key=True)
    product_id      = Column(Integer, ForeignKey('product_info.id'))
    barcode         = Column(String(256))
    compare_at_price        = Column(Integer)
    created_at      = Column(String(32))
    fulfillment_service     = Column(String(64))
    grams           = Column(Integer)
    inventory_management    = Column(String(64))
    inventory_policy        = Column(String(64))
    inventory_quantity      = Column(Integer)
    position        = Column(Integer)
    price           = Column(Integer)
    requires_shipping       = Column(Boolean)
    sku             = Column(String(32))
    taxable         = Column(Boolean)
    title           = Column(String(256))
    updated_at      = Column(String(32))
    image_id        = Column(Integer)
    option1         = Column(String(64))
    option2         = Column(String(64))
    option3         = Column(String(64))
    inventory_advance       = Column(String(32))

    product         = relationship('ProductInfo', foreign_keys=[product_id])


class ProductOptions(Base):
    __tablename__   = 'product_options'

    id              = Column(BigInteger, primary_key=True)
    product_id      = Column(Integer, ForeignKey('product_info.id'))
    name            = Column(String(256))
    position        = Column(Integer)

    product         = relationship('ProductInfo', foreign_keys=[product_id])
