import jwt
import uvicorn
import requests
from fastapi import FastAPI, Request, status, Depends, Response
from fastapi.responses import JSONResponse
from fastapi.responses import RedirectResponse
from urllib import parse
from time import sleep

from . import schemas, models, crud
from .database import engine, SessionLocal
from sqlalchemy.orm import Session

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

config = {
    'response_mode': 'form_post',
    'url_authorize': 'https://accounts.haravan.com/connect/authorize',
    'url_connect_token': 'https://accounts.haravan.com/connect/token',
    'grant_type': 'authorization_code',
    'nonce': 'abc123',
    'response_type': 'code id_token',
    'app_id': '5dce716c7702971691425d5f62488447',
    'app_secret': 'e8847bea3301ee60752cde0f9b2129591b04c76c10bc03dece5ad4ea133f399e',
    'scope_login': 'openid profile email org userinfo',
    'scope': 'openid profile email org userinfo grant_service com.write_products wh_api',
    'login_callback_url': 'http://localhost:8000/install/login',
    'install_callback_url': 'http://localhost:8000/install/grandservice',
    'webhook': {
        'hrVerifyToken': 'DZzohXO68eIwwnqSm843IEjfezwtaBAm',
        'subscribe': 'https://webhook.haravan.com/api/subscribe'
    }
}


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def dict_to_query(d):
    query = ''
    s = '='
    for key in d.keys():
        query += str(key) + '=' + str(d[key]) + '&'
    return query


def build_url_login():
    obj_query = {
        'response_mode': config['response_mode'],
        'response_type': config['response_type'],
        'scope': config['scope_login'],
        'client_id': config['app_id'],
        'redirect_uri': config['login_callback_url'],
        'nonce': config['nonce']
    }
    query = config['url_authorize'] + '?' + dict_to_query(obj_query)[:-1]
    return query


def build_url_install():
    obj_query = {
        'response_mode': config['response_mode'],
        'response_type': config['response_type'],
        'scope': config['scope'],
        'client_id': config['app_id'],
        'redirect_uri': config['install_callback_url'],
        'nonce': config['nonce']
    }
    query = config['url_authorize'] + '?' + dict_to_query(obj_query)[:-1]
    return query


def has_key(d, key):
    if key in d.keys():
        return True
    else:
        return False


def get_user_from_decode_jwt(params):
    try:
        userHR = jwt.decode(params, options={"verify_signature": False})
        if not isinstance(userHR, dict):
            return {'is_error': 'True',
                    'message': 'Get user info fail'}
        if not has_key(userHR, 'id'):
            userHR['id'] = userHR['sub']
        return userHR
    except:
        return {'is_error': 'True',
                'message': 'Get user info fail'}


def get_token(code):
    headers = {
        'Content-type': 'application/x-www-form-urlencoded',
        'Accept': 'text/plain'
    }
    param = {
        "code": code,
        "client_id": "5dce716c7702971691425d5f62488447",
        "client_secret": "e8847bea3301ee60752cde0f9b2129591b04c76c10bc03dece5ad4ea133f399e",
        "grant_type": "authorization_code",
        "redirect_uri": "http://localhost:8000/install/grandservice"
    }
    r = requests.post(config['url_connect_token'], data=param, headers=headers)
    return r.json()


def list_to_string(my_list):
    return ' '.join(my_list)


@app.get('/install/login')
async def login():
    url = build_url_login()
    return RedirectResponse(url)


@app.post('/install/login')
async def login(request: Request):
    body = await request.body()
    body_text = body.decode('utf-8')
    body_dict = dict(parse.parse_qs(body_text))
    id_token = list_to_string(body_dict['id_token'])
    userHR = get_user_from_decode_jwt(id_token)
    if has_key(userHR, 'is_error'):
        return JSONResponse(userHR['message'], status_code=status.HTTP_400_BAD_REQUEST)
    if not has_key(userHR, 'id') or not has_key(userHR, 'orgname'):
        return JSONResponse('Can not find user or org', status_code=status.HTTP_400_BAD_REQUEST)
    if 'admin' in userHR['role']:
        url = build_url_install()
        return RedirectResponse(url, status_code=status.HTTP_302_FOUND)
    # Remember to add status_code in Redirect Response to prevent error
    else:
        return JSONResponse('You are not authorized to access this page', status_code=status.HTTP_401_UNAUTHORIZED)


@app.post('/install/grandservice')
async def install_grandservice(request: Request, db: Session = Depends(get_db)):
    body = await request.body()
    try:
        if body == '':
            return JSONResponse('Code not found in request', status_code=status.HTTP_404_NOT_FOUND)
        body_text = body.decode('utf-8')
        body_dict = dict(parse.parse_qs(body_text))
        code = list_to_string(body_dict['code'])
        param_token = get_token(code)
        userHR = get_user_from_decode_jwt(param_token['id_token'])
        if has_key(userHR, 'is_error'):
            return JSONResponse(userHR['message'], status_code=status.HTTP_400_BAD_REQUEST)
        if not has_key(userHR, 'id') or not has_key(userHR, 'orgname'):
            return JSONResponse('Can not find user or org', status_code=status.HTTP_400_BAD_REQUEST)

        # Add authorize info to db
        authorizeInfo = models.AuthorizeInfo(access_token=param_token['access_token'],
                                             expires_in=param_token['expires_in'])
        crud.add_authorize_info(db, authorizeInfo)

        # Subscribe webhook
        subscribe(param_token['access_token'])

    except Exception as e:
        print(e)


# ---------------------------------------Webhook--------------------------------------- #

def subscribe(access_token):
    headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + access_token
    }
    r = requests.post(config['webhook']['subscribe'], headers=headers)
    if r:
        print('Subscribe successful')
    else:
        print('Subscribe unsuccessful')


@app.get('/webhooks')
async def webhook(request: Request):
    url = str(request.url)
    query_dict = dict(parse.parse_qs(parse.urlsplit(url).query))
    verify_token = list_to_string(query_dict['hub.verify_token'])
    hr_verify_token = config['webhook']['hrVerifyToken']
    hub_challenge = list_to_string(query_dict['hub.challenge'])
    print(hub_challenge)
    if verify_token != hr_verify_token:
        return Response(status_code=status.HTTP_401_UNAUTHORIZED)
    return Response(content=hub_challenge, status_code=status.HTTP_200_OK)


@app.post('/webhooks')
async def webhook(request: Request):
    body = await request.body()
    print(body)
    print()
    print(request.headers)
    return JSONResponse('')


@app.get('/get/shop')
async def get_shop():
    method = 'GET'
    data = ''
    url = 'https://apis.haravan.com/com/shop.json'
    r = send_api_request(url, method, data)
    return JSONResponse(r)


@app.get('/get/product')
async def get_product(db: Session = Depends(get_db)):
    method = 'GET'
    data = ''
    url = 'https://apis.haravan.com/com/products.json'
    r = send_api_request(url, method, data)
    product_list = r.get('products')
    for product in product_list:
        productInfo = models.ProductInfo(body_html=product['body_html'],
                                         body_plain=product['body_plain'],
                                         created_at=product['created_at'],
                                         handle=product['handle'],
                                         id=product['id'],
                                         product_type=product['product_type'],
                                         published_at=product['published_at'],
                                         published_scope=product['published_scope'],
                                         tags=product['tags'],
                                         template_suffix=product['template_suffix'],
                                         title=product['title'],
                                         updated_at=product['updated_at'],
                                         vendor=product['vendor'],
                                         only_hide_from_list=product['only_hide_from_list'],
                                         not_allow_promotion=product['not_allow_promotion']
                                         )
        db_product = crud.get_product_by_id(db, product['id'])
        if db_product:
            print('product exist')
        else:
            crud.add_product(db, productInfo)

        # Add images to product
        image_list = product.get('images')
        for image in image_list:
            imageInfo = models.ProductImages(created_at=image['created_at'],
                                             id=image['id'],
                                             position=image['position'],
                                             product_id=image['product_id'],
                                             src=image['src'],
                                             updated_at=image['updated_at'],
                                             filename=image['filename']
                                             )
            db_image = crud.get_image_by_id(db, image['id'])
            if db_image:
                print('image exist')
            else:
                crud.add_product_image(db, imageInfo)

        # Add variants to product
        variant_list = product.get('variants')
        for variant in variant_list:
            variantInfo = models.ProductVariants(id=variant['id'],
                                                 product_id=variant['product_id'],
                                                 barcode=variant['barcode'],
                                                 compare_at_price=variant['compare_at_price'],
                                                 created_at=variant['created_at'],
                                                 fulfillment_service=variant['fulfillment_service'],
                                                 grams=variant['grams'],
                                                 inventory_management=variant['inventory_management'],
                                                 inventory_policy=variant['inventory_policy'],
                                                 inventory_quantity=variant['inventory_quantity'],
                                                 position=variant['position'],
                                                 price=variant['price'],
                                                 requires_shipping=variant['requires_shipping'],
                                                 sku=variant['sku'],
                                                 taxable=variant['taxable'],
                                                 title=variant['title'],
                                                 updated_at=variant['updated_at'],
                                                 image_id=variant['image_id'],
                                                 option1=variant['option1'],
                                                 option2=variant['option2'],
                                                 option3=variant['option3'],
                                                 inventory_advance=variant['inventory_advance']
                                                 )
            db_variant = crud.get_variant_by_id(db, variant['id'])
            if db_variant:
                print('variant exist')
            else:
                crud.add_product_variant(db, variantInfo)

            # Add options to product
            option_list = product.get('options')
            # print(option_list)
            for option in option_list:
                print(option['id'])
                optionInfo = models.ProductOptions(id=option['id'],
                                                   product_id=option['product_id'],
                                                   name=option['name'],
                                                   position=option['position']
                                                   )
                db_option = crud.get_option_by_id(db, option['id'])
                if db_option:
                    print('option exist')
                else:
                    crud.add_product_option(db, optionInfo)
    return JSONResponse(r)


block = False  # Global variable use to control api limit rate


def send_api_request(url, method, data):
    global block
    access_token = '462BCCCFE76AA340A02FA558C4C4EE99633DFD67C327F633155A31085AFF6F96'
    headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + access_token
    }
    if block:
        sleep(30)
        block = False
    if method == 'GET':
        r = requests.get(url, headers=headers)
        # print(int(r.headers['X-Haravan-Api-Call-Limit'].split('/')[0]))
    elif method == 'POST':
        r = requests.post(url, headers=headers, data=data)
    elif method == 'DELETE':
        r = requests.delete(url, headers=headers)
    else:
        r = requests.put(url, headers=headers, data=data)
    if r.status_code == 200:
        return r.json()
    if r.status_code == 429:
        block = True
        sleep(20)
        if method == 'GET':
            r = requests.get(url, headers=headers)
        elif method == 'POST':
            r = requests.post(url, headers=headers, data=data)
        elif method == 'DELETE':
            r = requests.delete(url, headers=headers)
        else:
            r = requests.put(url, headers=headers, data=data)
        return r.json()


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8000)
