from pydantic import BaseModel
from typing import List


class AuthorizeInfo(BaseModel):
    access_token:   str
    expires_in:     str

    class Config:
        orm_mode = True


class ProductImage(BaseModel):
    created_at:     str
    id:             int # PK
    position:       int
    product_id:     int # FK
    src:            str
    updated_at:     str
    filename:       str

    class Config:
        orm_mode = True


class ProductVariant(BaseModel):
    id:             int # PK
    product_id:     int # FK
    barcode:        str
    compare_at_price:       int
    created_at:     str
    fulfillment_service:    str
    grams:          int
    inventory_management:   str
    inventory_policy:       str
    inventory_quantity:     int
    position:       int
    price:          int
    requires_shipping:      bool
    sku:            str
    taxable:        bool
    title:          str
    updated_at:     str
    image_id:       int
    option1:        str
    option2:        str
    option3:        str
    inventory_advance:      str

    class Config:
        orm_mode = True


class ProductOption(BaseModel):
    id:             int # PK
    product_id:     int # FK
    name:           str
    position:       int

    class Config:
        orm_mode = True


class ProductInfo(BaseModel):
    body_html:      str
    body_plain:     str
    created_at:     str
    handle:         str
    id:             int # PK
    images: List[ProductImage] = []
    product_type:   str
    published_at:   str
    published_scope:    str
    tags:           str
    template_suffix:    str
    title:          str
    updated_at:     str
    variants: List[ProductVariant] = []
    vendor:         str
    options: List[ProductOption] = []
    only_hide_from_list:    bool
    not_allow_promotion:    bool

    class Config:
        orm_mode = True
