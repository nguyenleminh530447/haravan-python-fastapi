from sqlalchemy.orm import Session
from . import models, schemas


def add_authorize_info(db: Session, authorize_info: schemas.AuthorizeInfo):
    db_info = models.AuthorizeInfo(access_token=authorize_info.access_token, expires_in=authorize_info.expires_in)
    db.add(db_info)
    db.commit()
    db.refresh(db_info)
    return db_info


def add_product(db: Session, product: schemas.ProductInfo):
    db_product = models.ProductInfo(body_html=product.body_html,
                                    body_plain=product.body_plain,
                                    created_at=product.created_at,
                                    handle=product.handle,
                                    id=product.id,
                                    product_type=product.product_type,
                                    published_at=product.published_at,
                                    published_scope=product.published_scope,
                                    tags=product.tags,
                                    template_suffix=product.template_suffix,
                                    title=product.title,
                                    updated_at=product.updated_at,
                                    vendor=product.vendor,
                                    only_hide_from_list=product.only_hide_from_list,
                                    not_allow_promotion=product.not_allow_promotion)
    db.add(db_product)
    db.commit()
    db.refresh(db_product)
    return db_product


def add_product_option(db: Session, option: schemas.ProductOption):
    db_option = models.ProductOptions(id=option.id,
                                      product_id=option.product_id,
                                      name=option.name,
                                      position=option.position)
    db.add(db_option)
    db.commit()
    db.refresh(db_option)
    return db_option


def add_product_variant(db: Session, variant: schemas.ProductVariant):
    db_variant = models.ProductVariants(id=variant.id,
                                        product_id=variant.product_id,
                                        barcode=variant.barcode,
                                        compare_at_price=variant.compare_at_price,
                                        created_at=variant.created_at,
                                        fulfillment_service=variant.fulfillment_service,
                                        grams=variant.grams,
                                        inventory_management=variant.inventory_management,
                                        inventory_policy=variant.inventory_policy,
                                        inventory_quantity=variant.inventory_quantity,
                                        position=variant.position,
                                        price=variant.price,
                                        requires_shipping=variant.requires_shipping,
                                        sku=variant.sku,
                                        taxable=variant.taxable,
                                        title=variant.title,
                                        updated_at=variant.updated_at,
                                        image_id=variant.image_id,
                                        option1=variant.option1,
                                        option2=variant.option2,
                                        option3=variant.option3,
                                        inventory_advance=variant.inventory_advance)
    db.add(db_variant)
    db.commit()
    db.refresh(db_variant)
    return db_variant


def add_product_image(db: Session, image: schemas.ProductImage):
    db_image = models.ProductImages(created_at=image.created_at,
                                    id=image.id,
                                    position=image.position,
                                    product_id=image.product_id,
                                    src=image.src,
                                    updated_at=image.updated_at,
                                    filename=image.filename)
    db.add(db_image)
    db.commit()
    db.refresh(db_image)
    return db_image


def get_product_by_id(db: Session, product_id: int):
    return db.query(models.ProductInfo).filter(models.ProductInfo.id == product_id).first()


def get_image_by_id(db: Session, image_id: int):
    return db.query(models.ProductImages).filter(models.ProductImages.id == image_id).first()


def get_variant_by_id(db: Session, variant_id: int):
    return db.query(models.ProductVariants).filter(models.ProductVariants.id == variant_id).first()


def get_option_by_id(db: Session, option_id: int):
    return db.query(models.ProductOptions).filter(models.ProductOptions.id == option_id).first()
